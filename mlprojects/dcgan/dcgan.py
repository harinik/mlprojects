"""
DCGAN implementation using Keras, trained on a custom dataset.

Inspired by the Tensorflow tutorial on DCGAN and various other online resources.
"""
import matplotlib.pyplot as plt
import numpy as np
import os
from PIL import Image
import tensorflow as tf
from tensorflow import keras


def generator_model(noise_dim):
    """
    Generator model
    :param noise_dim: The size of the noise vector from which images are generated. The generator is designed to
    generate images of size (32, 32, 3).
    :return: the generator model
    """
    model = keras.Sequential()
    model.add(keras.layers.Dense(4*4*512, input_shape=(noise_dim,)))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation(tf.nn.tanh))
    model.add(keras.layers.Reshape((4, 4, 512)))
    assert model.output_shape == (None, 4, 4, 512)

    model.add(keras.layers.Conv2DTranspose(256, (5, 5), strides=(2, 2), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation(tf.nn.tanh))
    assert model.output_shape == (None, 8, 8, 256)

    model.add(keras.layers.Conv2DTranspose(128, (5, 5), strides=(2, 2), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation(tf.nn.tanh))
    assert model.output_shape == (None, 16, 16, 128)

    model.add(keras.layers.Conv2DTranspose(64, (5, 5), strides=(2, 2), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation(tf.nn.tanh))
    assert model.output_shape == (None, 32, 32, 64)

    model.add(keras.layers.Conv2DTranspose(32, (5, 5), strides=(1, 1), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation(tf.nn.tanh))
    assert model.output_shape == (None, 32, 32, 32)

    model.add(keras.layers.Conv2DTranspose(3, (5, 5), strides=(1, 1), padding='same'))
    model.add(keras.layers.Activation(tf.nn.tanh))
    assert model.output_shape == (None, 32, 32, 3)

    return model


def discriminator_model(input_dims):
    """
    Discriminator model
    :param input_dims: The dimensions of the input image.
    :return: the discriminator model
    """
    model = keras.Sequential()
    model.add(keras.layers.Conv2D(32, (5, 5), strides=(2, 2), input_shape=input_dims, padding='same'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(keras.layers.Activation(tf.nn.tanh))

    model.add(keras.layers.Conv2D(64, (5, 5), padding='same'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(keras.layers.Activation(tf.nn.tanh))

    model.add(keras.layers.Conv2D(128, (5, 5), padding='same'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(keras.layers.Activation(tf.nn.tanh))

    model.add(keras.layers.Conv2D(256, (5, 5), padding='same'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(keras.layers.Activation(tf.nn.tanh))

    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(1))
    model.add(keras.layers.Activation(tf.nn.sigmoid))

    return model


def combined_gen_disc_model(gen, disc):
    """
    A combined generator and discriminator model that feeds the output of the generator to the discriminator.
    This is used to minimize the generator loss. The goal is for the generator output to fool the discriminator i.e.
    discriminator classifies them as real.
    :param gen: The generator model
    :param disc: The discriminator model
    :return: The combined model used to minimize the generator loss.
    """
    model = keras.Sequential()
    model.add(gen)
    disc.trainable = False
    model.add(disc)
    return model


def train(dataset, num_epochs, batch_size, noise_dim):
    """
    Train the GAN
    :param dataset: The dataset of images
    :param num_epochs: Number of epochs
    :param batch_size: Batch size
    :param noise_dim: Size of the noise vector from which the generator generates images.
    :return:
    """
    generator = generator_model(noise_dim)
    generator.compile(loss='binary_crossentropy', optimizer='adam')

    discriminator = discriminator_model((32, 32, 3))

    cgd = combined_gen_disc_model(generator, discriminator)
    cgd.compile(loss='binary_crossentropy', optimizer='SGD')

    discriminator.trainable = True
    discriminator.compile(loss='binary_crossentropy', optimizer='adam')

    dloss = []
    gloss = []
    for epoch in range(num_epochs):
        # shuffle the dataset during each epoch
        np.random.shuffle(dataset)
        for i in range(int(dataset.shape[0]/batch_size)):
            data_batch = dataset[i*batch_size:(i+1)*batch_size]
            noise = np.random.normal(0, 1, (batch_size, noise_dim))

            gen_images = generator.predict(noise)

            real_loss = discriminator.train_on_batch(data_batch, np.ones((batch_size, 1)))
            fake_loss = discriminator.train_on_batch(gen_images, np.zeros((batch_size, 1)))
            disc_loss = real_loss + fake_loss

            gen_loss = cgd.train_on_batch(noise, np.ones((batch_size, 1)))

        dloss.append(disc_loss)
        gloss.append(gen_loss)
        if epoch % 100 == 0:
            print('Epoch: %d Disc loss: %.2f Gen loss: %.2f' % (epoch, disc_loss, gen_loss))
            generate_and_show_images(generator, noise_dim)

    plot_losses(dloss, gloss)

    # show generated images after the training is done.
    generate_and_show_images(generator, noise_dim)


def plot_losses(dloss, gloss):
    """
    Plot the discriminator and generator losses.
    """
    plt.plot(dloss)
    plt.plot(gloss)
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend(['Discriminator loss', 'Generator loss'], loc='upper left')
    plt.show()


def generate_and_show_images(model, noise_dim):
    """
    Shows the images generated by the generator.
    :param model: The model used to generate the images.
    :param noise_dim: The size of the noise vector from which images are generated.
    """
    test_input = np.random.normal(0, 1, [16, noise_dim])
    preds = model.predict(test_input)
    plt.figure(figsize=(4, 4))

    for i in range(preds.shape[0]):
        plt.subplot(4, 4, i + 1)
        img = preds[i] * 127.5 + 127.5
        plt.imshow(img.astype(np.uint8))
        plt.axis('off')

    plt.show()


def resize_and_save(srcdir, destdir, ih, iw):
    if os.path.exists(destdir) and len(os.listdir(destdir)) > 0:
        return
    if not os.path.exists(destdir):
        os.mkdir(destdir)

    for f in os.listdir(srcdir):
        fpath = os.path.join(srcdir, f)
        img = Image.open(fpath)
        img = img.resize((ih, iw))
        img.save(os.path.join(destdir, f))


def read_data(datadir):
    imgs = []
    for f in os.listdir(datadir):
        img = plt.imread(os.path.join(datadir, f))
        imgs.append(img)
    return np.array(imgs)


def get_dataset(datadir):
    train_x = read_data(datadir)
    print('train_x.shape: ', train_x.shape)
    train_x = train_x.reshape(train_x.shape[0], 32, 32, 3).astype('float32')
    train_x = (train_x - 127.5) / 127.5  # Normalize the images to [-1, 1]

    return train_x


def run_train(datadir, num_epochs, batch_size):
    ds = get_dataset(datadir)
    train(ds, num_epochs, batch_size, 100)


