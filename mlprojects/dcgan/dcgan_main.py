import argparse
import dcgan
import os


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--num_epochs', '-e', type=int, default=10, help='Number of epochs')
    parser.add_argument('--batch_size', '-b', type=int, default=128, help='Batch size')
    parser.add_argument('datadir', type=str, help='Path to data files')
    parser.add_argument('destdir', type=str, help='Dir where resized files are stored.')
    args = parser.parse_args()
    # resize image files to specified size.
    dcgan.resize_and_save(args.datadir, args.destdir, 32, 32)
    assert len(os.listdir(args.destdir)) > 0
    dcgan.run_train(args.destdir, args.num_epochs, args.batch_size)
