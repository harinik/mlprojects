"""
Example of building a DNN using tensorflow.
"""
import logging
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import data_splitter as ds
import skimage.transform as skt
import keras_utils


def create_model(X, nn_layer_dims):
    """
    Creates the neural network model.
    :param X: input data
    :param nn_layer_dims: A list containing the number of hidden units in each layer of the neural network (does not
    include the input)
    :return: The final linear layer of the constructed neural network
    """
    initializer = tf.contrib.layers.xavier_initializer()
    layer = tf.layers.flatten(X)
    print("X.shape: ", X.shape, "; layer.shape: ", layer.shape)
    # create a fully connected neural network with ReLU activation for all layers except the last one.
    for i in range(len(nn_layer_dims) - 1):
        layer = tf.layers.dense(layer, nn_layer_dims[i], activation=tf.nn.relu, kernel_initializer=initializer,
                                name='Layer' + str(i))

    i = len(nn_layer_dims) - 1
    # For the final layer, only the linear unit is created here (i.e. no ReLU/sigmoid activation) - the activation
    # function is applied while computing the cross entropy (this is apparently more efficient in tensorflow).
    output_layer = tf.layers.dense(layer, nn_layer_dims[i], activation=None, name='output')
    return output_layer


def train(sess, datasets, nn_layer_dims, num_classes,
          learning_rate=0.005, num_epochs=1000, mini_batch_size=128):
    """
    Trains the neural network.
    :param nn_layer_dims: A list of the number of hidden units in each layer of the neural network to be created.
    :param learning_rate: learning rate while training the model.
    :param num_epochs: The number of epochs to run the training for. One epoch corresponds to one pass over the entire
    training data.
    :param mini_batch_size: The mini-batch size to split the training data into.
    :return:
    """
    # get the training, dev and test data sets
    train_x = datasets['train_x']
    dev_x = datasets['dev_x']
    test_x = datasets['test_x']
    train_y = keras_utils.convert_to_one_hot(datasets['train_y'], num_classes)
    test_y = keras_utils.convert_to_one_hot(datasets['test_y'], num_classes)
    dev_y = keras_utils.convert_to_one_hot(datasets['dev_y'], num_classes)

    logging.info('train_x shape: %s', train_x.shape)
    logging.info('train_y.shape: %s', train_y.shape)
    logging.info('test_x shape: %s', test_x.shape)
    logging.info('test_y.shape: %s', test_y.shape)
    logging.info('dev_x shape: %s', dev_x.shape)
    logging.info('dev_y.shape: %s', dev_y.shape)

    # create placeholders for the training data and labels.
    x = tf.placeholder(name='Input', shape=[None, train_x.shape[1], train_x.shape[2], train_x.shape[3]], dtype=tf.float32)
    y = tf.placeholder(name='Label', shape=[None, train_y.shape[1]], dtype=tf.float32)

    nn_layer_dims.append(num_classes)
    # create the neural network and get the final layer.
    output_layer = create_model(x, nn_layer_dims)

    # apply the softmax activation function and calculate the cost.
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(logits=output_layer, labels=y)
    cost = tf.reduce_mean(cross_entropy)

    # define the optimizer used during the training to minimize the cost.
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    pred = tf.argmax(output_layer, 1, name='Pred')
    expected = tf.argmax(y, 1)

    # List to keep track of how the cost varies through the training.
    cost_history = []

    # initialize tf variables.
    init = tf.global_variables_initializer()
    sess.run(init)

    # set a seed so results are consistent.
    tf.set_random_seed(1)

    for epoch in range(num_epochs):
        total_cost = 0.
        # create mini-batches
        mini_batches = ds.create_mini_batches(train_x, train_y, mini_batch_size)
        for mb in mini_batches:
            (mb_x, mb_y) = mb
            # run the optimizer and get the cost for the mini-batch.
            _, mb_cost = sess.run([optimizer, cost], feed_dict={x: mb_x, y: mb_y})
            total_cost += (mb_cost / len(mini_batches))

        if epoch % 100 == 0:
            logging.info('Cost after epoch %i: %f', epoch, total_cost)
        if epoch % 10 == 0:
            cost_history.append(total_cost)

    plt.plot(np.squeeze(cost_history))
    plt.ylabel('cost')
    plt.xlabel('iterations (per tens)')
    plt.title('Learning rate =' + str(learning_rate))
    plt.show()

    # apply the activation function to the final layer of the NN to get the predicted value.
    # and check if this is a correct predicted i.e. predicted value equal to true label
    correct_pred = tf.equal(pred, expected)

    # calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    train_accuracy = sess.run(accuracy, feed_dict={x: train_x, y: train_y})
    dev_accuracy = sess.run(accuracy, feed_dict={x: dev_x, y: dev_y})
    test_accuracy = sess.run(accuracy, feed_dict={x: test_x, y: test_y})
    logging.info('Train accuracy: %f; Dev accuracy: %f; Test accuracy: %f', train_accuracy, dev_accuracy, test_accuracy)


def predict(sess, img_file_name, img_height, img_width):
    """
    Get the prediction for a given image (to be read from a file).
    :param sess: The tensorflow session we are operating in. This is needed to get the weights for the model we just
    trained, and the appropriate tensors to get the prediction from.
    :param img_file_name: The image file for which we want to predict the category.
    :param img_height: The height of the image to resize to.
    :param img_width: The width of the image to resize to.
    :return: The predicted value for the image.
    """
    img = plt.imread(img_file_name, format='jpeg')
    plt.show(img)
    img = skt.resize(img, (img_height, img_width))
    img = np.expand_dims(img, axis=0)
    if len(img.shape) != 4:
        img = img.reshape(img.shape[0], img.shape[1], img.shape[2], 1)

    # get the tensor which makes the prediction.
    pred = sess.graph.get_tensor_by_name('Pred:0')
    # run the prediction for the image.
    prediction = sess.run(pred, feed_dict={'Input:0': img})

    return np.squeeze(prediction)
