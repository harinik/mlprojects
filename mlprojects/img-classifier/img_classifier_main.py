
import sys
sys.path.append('../utils')

import argparse
import data_splitter as ds
import json
from tensorflow import keras
import os
import img_classifier_dnn as kd
import img_classifier_lenet as ln
import img_classifier_alexnet as al
import keras_utils

import logging


def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_type', type=str, help='Type of NN model for image classification. Valid choices are: '
                                                     'dnn, lenet, alexnet or predict (for prediction)',
                        choices=['dnn', 'lenet', 'alexnet', 'predict'])
    parser.add_argument('--dataset_root', type=str, help='Path where the dataset is located.')
    parser.add_argument('--train_dir', type=str, help='Name of dir where training data is located under dataset_root',
                        default='train')
    parser.add_argument('--test_dir', type=str, help='Name of dir where test data is located under dataset_root',
                        default='test')
    parser.add_argument('--dev_dir', type=str, help='Name of dir where dev data is located under dataset_root',
                        default='dev')

    parser.add_argument('--prediction_file', type=str, help='File to run prediction on.')
    parser.add_argument('-ih', '--img_height', type=int, default=32, help='Height of the iamge')
    parser.add_argument('-iw', '--img_width', type=int, default=32, help='Width of the image')
    parser.add_argument('-b', '--batch_size', type=int, help='batch size during training', default=64)
    parser.add_argument('-l', '--logfile', default='training.log', help='Log file to write to.')

    parser.add_argument('-e', '--num_epochs', type=int, default=10,
                        help='Number of epochs to run the training set for.')
    parser.add_argument('--dnn_layers', help='List of the number of layers in the deep NN, excluding the last softmax'
                                             'layer. Does not apply for CNN.',
                        nargs='+', type=int)
    parser.add_argument('model_file', type=str, help='File to save the model or load from, if it already exists.')
    parser.add_argument('labels_file', type=str,
                        help='File to write JSON representation of the human-readable labels')
    return parser.parse_args()


def classifier_main():
    args = read_args()
    print(args)
    logging.basicConfig(filename=args.logfile, level=logging.INFO)
    if args.model_type != 'predict':
        if args.dataset_root is None:
            print('Dataset root must be present except for model type=predict')
            return

        datasets, categories = ds.get_datasets_and_categories(os.path.join(args.dataset_root, args.train_dir),
                                                              os.path.join(args.dataset_root, args.test_dir),
                                                              os.path.join(args.dataset_root, args.dev_dir))
        print(categories)
        cat_json = json.dumps(categories)
        with open(args.labels_file, 'w') as cf:
            json.dump(cat_json, cf)

        model = None
        if args.model_type == 'lenet':
            model = ln.train(datasets, num_classes=len(categories), num_epochs=args.num_epochs,
                             batch_size=args.batch_size)
        elif args.model_type == 'dnn':
            model = kd.train(datasets, args.dnn_layers, num_classes=len(categories), num_epochs=args.num_epochs,
                             batch_size=args.batch_size)
        elif args.model_type == 'alexnet':
            model = al.train(datasets, num_classes=len(categories), num_epochs=args.num_epochs,
                             batch_size=args.batch_size)

        if model is not None:
            model.save(args.model_file)
    else:
        with open(args.labels_file) as cf:
            cat_json = json.load(cf)
        categories = json.loads(cat_json)
        print(categories)
        if args.prediction_file is None:
            print('Prediction file must be specified.')
            return
        model = keras.models.load_model(args.model_file)

        pred = keras_utils.keras_img_predict(model, args.prediction_file, args.img_height, args.img_width)
        print('Input file:', args.prediction_file)
        print('Predicted value:', categories[pred])


if __name__ == '__main__':
    classifier_main()