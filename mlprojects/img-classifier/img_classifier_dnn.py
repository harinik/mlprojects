import logging
import tensorflow as tf
from tensorflow import keras
import keras_utils


def create_model(X, nn_layer_dims):
    """
    Creates the neural network model.
    :param X: input data
    :param nn_layer_dims: A list containing the number of hidden units in each layer of the neural network (does not
    include the input)
    :return: The constructed model
    """
    model = keras.Sequential()
    # flatten the input images into a 1D representation.
    model.add(keras.layers.Flatten(input_shape=X.shape[1:]))
    for i in range(len(nn_layer_dims) - 1):
        model.add(keras.layers.Dense(nn_layer_dims[i], activation=tf.nn.relu))
    model.add(keras.layers.Dense(nn_layer_dims[i+1], activation=tf.nn.softmax))
    return model


def train(datasets, nn_layer_dims, num_classes, num_epochs=100, batch_size=64):
    """
    Trains the neural network.
    :param datasets: Datasets containing training, test and validation(dev) sets.
    :param nn_layer_dims: A list of the number of hidden units in each layer of the neural network to be created.
    :param num_epochs: The number of epochs to run the training for. One epoch corresponds to one pass over the entire
    training data.
    :param batch_size: The mini-batch size to split the training data into.
    :return:
    """
    # get the training, dev and test data sets
    train_x = datasets['train_x']
    dev_x = datasets['dev_x']
    test_x = datasets['test_x']
    train_y = keras_utils.convert_to_one_hot(datasets['train_y'], num_classes)
    test_y = keras_utils.convert_to_one_hot(datasets['test_y'], num_classes)
    dev_y = keras_utils.convert_to_one_hot(datasets['dev_y'], num_classes)

    logging.info('train_x shape: %s', train_x.shape)
    logging.info('train_y.shape: %s', train_y.shape)
    logging.info('test_x shape: %s', test_x.shape)
    logging.info('test_y.shape: %s', test_y.shape)
    logging.info('dev_x shape: %s', dev_x.shape)
    logging.info('dev_y.shape: %s', dev_y.shape)

    nn_layer_dims.append(num_classes)
    model = create_model(train_x, nn_layer_dims)
    model.summary()
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    history = model.fit(train_x, train_y, batch_size=batch_size, epochs=num_epochs,
                        validation_data=(dev_x, dev_y), verbose=2)

    # Plot training & validation accuracy values
    keras_utils.show_keras_graph(history)

    test_loss, test_acc = model.evaluate(test_x, test_y)
    logging.info('test loss: %s test accuracy: %s', test_loss, test_acc)

    return model
