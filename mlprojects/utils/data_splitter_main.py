"""
Tool to split an image dataset into train, test and dev sets.
"""
import argparse
import data_splitter as ds


def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('src_dir', type=str, help='Directory where source data is located.')
    parser.add_argument('dest_dir', type=str, help='Destination directory where train, test and dev data are stored.')
    parser.add_argument('--train_fraction', type=float, default=0.8, help='Fraction of data used for training.')
    parser.add_argument('-ih', '--image_height', type=int, default=32, help='Image height to resize to.')
    parser.add_argument('-iw', '--image_width', type=int, default=32, help='Image height to resize to.')
    return parser.parse_args()


if __name__ == '__main__':
    args = read_args()
    ds.create_dataset(args.src_dir, args.dest_dir, args.train_fraction, args.image_height, args.image_width)
