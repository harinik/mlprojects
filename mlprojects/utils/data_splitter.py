"""
Methods to create train/test/dev split of image data from a single large dataset.
The dataset is assumed to be stored in a directory where each subdir is a label name and all the files under it
are associated with that label.
For example:

mnist/
|__0/
    |__img_0.jpg, img_1.jpg, ...
|__1/
    |__...

"""
import math
import matplotlib.pyplot as plt
import numpy as np
import os
from PIL import Image
import random


def get_categories(dir):
    """
    Walks through the directory hierarchy and a sorted list of labels.
    The assumption is that the parent directory contains subdirectories, each of
    which contains data associated with a given label and that the name of the subdirectory is the label.
    For instance,
    parent
    |___label1
    |___label2
    |___label3
    :param dir: parent directory where the dataset is located.
    :return: sorted list of labels [label1, label2, label3]
    """
    categories = []
    for subdir in sorted(os.listdir(dir)):
      categories.append(subdir)
    return sorted(categories)


def files_and_labels(dir):
    """
    Creates a list of tuples of file paths and the label associated with that file.
    :param dir: the directory to get the file paths and labels. It is assumed to have the structured outlined in the
    top-level comment.
    :return: A list of tuples of file paths and labels.
    """
    files_lbls = []
    for sd in sorted(os.listdir(dir)):
        sdpath = os.path.join(dir, sd)
        for f in os.listdir(sdpath):
            files_lbls.append((os.path.join(sdpath, f), sd))
    return files_lbls


def shuffle_and_split(files_and_lbls, train_fraction):
    """
    Shuffles and splits a list of tuples of file paths and labels into train/test/dev sets.
    :param files_and_lbls: A list of tuples of file paths and labels e.g. [(file1, label1), (file2, label2), ...]
    :param train_fraction: The fraction of data to be used for training.
    :return: A dict that maps train, test and dev to a list of tuples of file paths and labels.
    """
    files_and_lbls.sort()
    random.seed(1)
    random.shuffle(files_and_lbls)
    dev_index = int(train_fraction * len(files_and_lbls))
    dev_fraction = (1 - train_fraction) / 2.0
    test_index = int(dev_index + dev_fraction * len(files_and_lbls))
    train_files = files_and_lbls[:dev_index]
    dev_files = files_and_lbls[dev_index:test_index]
    test_files = files_and_lbls[test_index:]
    datafiles = {
        'train': train_files,
        'test': test_files,
        'dev': dev_files
    }
    return datafiles


def resize_and_save(datafiles, output_dir, ih, iw):
    """
    Resizes and saves the train/test/dev image data.
    :param datafiles: A dict containing the train, test and dev file paths and labels.
    :param output_dir: The destination directory where the resized image bytes should be saved. A train/test/dev
    hierarchy will be created under output_dir.
    :param ih: image height to resize to.
    :param iw: image width to resize to.
    """
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for dstype, fs in datafiles.items():
        destdir = os.path.join(output_dir, dstype)
        if not os.path.exists(destdir):
            os.mkdir(destdir)

        for item in fs:
            fname = item[0]
            img = Image.open(fname)
            img = img.resize((ih, iw))
            # prefix the label name to the file name so that it can be inferred while reading back the file.
            newfname = item[1] + '_' + os.path.basename(fname)
            img.save(os.path.join(destdir, newfname))


def create_dataset(src, dest, train_fraction, ih, iw):
    """
    Creates the dataset with the train/test/dev split in the dest directory, from the data in the src directory.
    :param src: Source directory where the data is stored.
    :param dest: Destination directory where train, test and dev data are to be stored.
    :param train_fraction: Fraction of data to use for training.
    :param ih: image height to resize to.
    :param iw: image width to resize to.
    """
    files_lbls = files_and_labels(src)
    datafiles = shuffle_and_split(files_lbls, train_fraction)
    resize_and_save(datafiles, dest, ih, iw)


def get_image_data(dir):
    """
    Reads images and labels from the specified directory as numpy arrays.
    :param dir: Directory to read image data from.
    :return: image data and label data as numpy arrays, as well as a sorted list of human-readable label names.
    """
    imgs = []
    lblnames = []
    for f in os.listdir(dir):
        lblnames.append(f.split('_')[0])
        img = plt.imread(os.path.join(dir, f))
        img = img / 255.0
        imgs.append(img)

    lblset = set(lblnames)
    lbllist = sorted(list(lblset))
    lbls = []
    for ln in lblnames:
        lbls.extend([lbllist.index(ln)])

    imgs = np.array(imgs)
    if len(imgs.shape) != 4:
        imgs = imgs.reshape(imgs.shape[0], imgs.shape[1], imgs.shape[2], 1)
    lbls = np.array(lbls)

    return imgs, lbls, lbllist


def get_datasets_and_categories(train_dir, test_dir, dev_dir):
    """
    Return the train, test and dev datasets (numpy arrays of images and labels), as well as a list of human-readable
    label names.
    :param train_dir: Directory where training data is stored.
    :param test_dir: Directory where test data is stored.
    :param dev_dir: Directory where dev data is stored.
    :return: A dict containing the training, test and dev data and labels, as well as a list of the label names.
    """
    datasets = {}
    datasets['train_x'], datasets['train_y'], train_lbls = get_image_data(train_dir)
    datasets['test_x'], datasets['test_y'], test_lbls = get_image_data(test_dir)
    datasets['dev_x'], datasets['dev_y'], dev_lbls = get_image_data(dev_dir)
    # put all the label names into a single big list
    categories = []
    for lbls in [train_lbls, test_lbls, dev_lbls]:
        categories.extend(lbls)
    # convert the list to a set to uniquify, then back to a list and sort.
    categories = sorted(list(set(categories)))
    return datasets, categories


def create_mini_batches(X, Y, mini_batch_size):
    """
    Creates mini-batches from the provided data with the specified batch sizes. This is primarily for tensorflow
    models.
    :param X: input data
    :param Y: labels
    :param mini_batch_size: batch size
    :return: A list of mini-batches where each mini-batch is (mx, my) where len(mx) <= mini_batch_size.
    """
    mini_batches = []
    num_samples = X.shape[0]
    num_mini_batches = int(math.floor(num_samples/mini_batch_size))
    for i in range(num_mini_batches):
        mini_batch_X = X[i * mini_batch_size : (i+1) * mini_batch_size, :]
        mini_batch_Y = Y[i * mini_batch_size : (i+1) * mini_batch_size, :]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)

    if num_samples % num_mini_batches > 0:
        mini_batch_X = X[num_mini_batches * mini_batch_size : num_samples, :]
        mini_batch_Y = Y[num_mini_batches * mini_batch_size : num_samples, :]
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)

    return mini_batches



