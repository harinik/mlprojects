import csv
import shutil
import os


def get_cifar10_labeled_data(csvfile, srcdir, destroot):
    """
    Reads the trainLabels csv file that maps the input images to labels. Copies the image to a destination dir with
    format root/label e.g. data/airplane, where label is the label associated with the image. This is really a one-off
    method needed to handle cifar10 dataset downloaded from Kaggle.
    This results in a structure on disk that can be accessed via the utils module.
    :param csvfile: csv file with format (img number, label). Image number corresponds to the file name as well.
    :param srcdir: Source directory where images live.
    :param destroot: Destination directory to store images categorized by label.
    """
    if not os.path.exists(destroot):
        os.mkdir(destroot)
    with open(csvfile, newline='') as csvf:
        cr = csv.reader(csvf)
        # skip header
        next(cr)
        for row in cr:
            fname = row[0] + '.png'
            label = row[1]
            srcpath = os.path.join(srcdir, fname)
            destdir = os.path.join(destroot, label)
            if not os.path.exists(destdir):
                os.mkdir(destdir)
            destpath = os.path.join(destdir, fname)
            shutil.copyfile(srcpath, destpath)



