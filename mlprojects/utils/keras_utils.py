import numpy as np
import matplotlib.pyplot as plt
import skimage.transform as skt


def convert_to_one_hot(y, num_classes):
    """Convert the labels into one-hot encoding.
    e.g. If the number of classes is 3 and the labels are 0, 1, 2, one-hot encoding represents them as 100, 010, 001
    respectively. Tensorflow offers a tf.one_hot method but here, we use numpy directly since we want to operate
    directly on the array instead of converting it into a tensor.
    :param y: The labels that need to be encoded.
    :param num_classes: The number of classes for the labels.
    """
    return np.eye(num_classes)[y.reshape(-1)]


def show_keras_graph(history):
    """
    Plot the graph obtained by reading keras history values after training.
    :param history: keras history object
    """
    # Plot training & validation accuracy values
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()


def keras_img_predict(model, img_file_name, img_height, img_width):
    """
    Get the prediction for a given image (to be read from a file).
    :param model: The keras model to use for predictions.
    :param img_file_name: The image file for which we want to predict the category.
    :param img_height: The height of the image to resize to.
    :param img_width: The width of the image to resize to.
    :return: The predicted value for the image.
    """
    img = plt.imread(img_file_name)
    img = skt.resize(img, (img_height, img_width))
    plt.imshow(img)
    plt.show()
    img = np.expand_dims(img, axis=0)
    if len(img.shape) != 4:
        img = img.reshape(img.shape[0], img.shape[1], img.shape[2], 1)
    pred = model.predict(img)
    return np.argmax(pred)