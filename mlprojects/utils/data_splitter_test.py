import numpy as np
import os
import unittest
import tempfile
import data_splitter as ds
from PIL import Image
import shutil


class TestDataSplitter(unittest.TestCase):

    def testGetCategories(self):
        labels = ['foo', 'bar', 'baz']
        dir = create_source_data(labels, numfiles=0)
        categories = ds.get_categories(dir)
        self.assertListEqual(sorted(labels), categories)
        shutil.rmtree(dir)

    def testGetFilesAndLabels(self):
        labels = ['foo', 'bar', 'baz']
        numfiles = 2
        dir = create_source_data(labels, numfiles)
        fl = ds.files_and_labels(dir)
        self.assertEqual(len(fl), len(labels) * numfiles)
        shutil.rmtree(dir)

    def testShuffleAndSplit(self):
        labels = ['foo', 'bar', 'baz']
        numfiles = 10
        train_frac = 0.8
        dir = create_source_data(labels, numfiles)
        fl = ds.files_and_labels(dir)
        datafiles = ds.shuffle_and_split(fl, train_frac)
        self.assertEqual(len(datafiles['train']), round(train_frac * len(labels) * numfiles))
        test_frac = dev_frac = (1 - train_frac) / 2.0
        self.assertEqual(len(datafiles['test']), round(test_frac * len(labels) * numfiles))
        self.assertEqual(len(datafiles['dev']), round(dev_frac * len(labels) * numfiles))
        shutil.rmtree(dir)

    def testCreateDataset(self):
        labels = ['foo', 'bar', 'baz']
        srcdir = create_source_data(labels, 10)
        destdir = tempfile.mkdtemp(prefix='dest')

        target_size = 16
        ds.create_dataset(srcdir, destdir, train_fraction=0.8, ih=target_size, iw=target_size)

        train_dir = os.path.join(destdir, 'train')
        self.assertTrue(os.path.exists(train_dir))

        f = os.listdir(train_dir)[0]
        self.assertTrue(f.split('_')[0] in labels)
        img = Image.open(os.path.join(train_dir, f))
        self.assertEqual(img.size[0], target_size)
        self.assertEqual(img.size[1], target_size)

        shutil.rmtree(srcdir)
        shutil.rmtree(destdir)

    def testCreateMiniBatches(self):
        X = np.random.rand(107, 5)
        Y = np.random.rand(107, 1)

        mini_batches = ds.create_mini_batches(X, Y, 25)
        self.assertEqual(5, len(mini_batches))
        self.assertEqual(25, len(mini_batches[0][0]))
        self.assertEqual(7, len(mini_batches[4][0]))

    def testGetDatasets(self):
        labels = ['foo', 'bar', 'baz']
        numfiles=10
        srcdir = create_source_data(labels, numfiles)
        destdir = tempfile.mkdtemp(prefix='dest')

        target_size = 16
        train_fraction = 0.8
        ds.create_dataset(srcdir, destdir, train_fraction=train_fraction, ih=target_size, iw=target_size)

        datasets, categories = ds.get_datasets_and_categories(os.path.join(destdir, 'train'),
                                                              os.path.join(destdir, 'test'),
                                                              os.path.join(destdir, 'dev'))
        self.assertListEqual(sorted(labels), sorted(categories))
        self.assertEqual(datasets['train_x'].shape, (round(train_fraction * len(labels) * numfiles),
                                                      target_size, target_size, 3))
        shutil.rmtree(srcdir)
        shutil.rmtree(destdir)


def create_source_data(labels, numfiles, ih=32, iw=32):
    dir = tempfile.mkdtemp(prefix='data', dir='/var/tmp')

    for l in labels:
        ldir = os.path.join(dir, l)
        os.mkdir(ldir)
        for i in range(numfiles):
            create_image_file(ldir, ih, iw)
    return dir


def create_image_file(dir, h, w, prefix=None):
    img = Image.new('RGB', size=(h, w))
    f = tempfile.NamedTemporaryFile(prefix=prefix, suffix='.jpg', dir=dir, delete=False)
    img.save(f)


if __name__ == '__main__':
    unittest.main()