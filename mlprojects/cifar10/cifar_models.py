"""
Keras models for classification with the cifar10 dataset.
"""

import modelbase as mb
import tensorflow as tf
from tensorflow import keras


CATEGORIES = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']


class DNN1(mb.ImgClassifierModel):
    """
    A fully connected deep neural network model.
    """
    def __init__(self, datasets, num_classes, config):
        super().__init__(datasets, num_classes, config)
        self.model_name = 'DNN1'

        model = keras.Sequential()
        datasets = self.config['datasets']
        X = datasets['train_x']
        # flatten the input images into a 1D representation.
        model.add(keras.layers.Flatten(input_shape=X.shape[1:]))
        model.add(keras.layers.Dense(32, activation=tf.nn.relu))
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(32, activation=tf.nn.relu))
        model.add(keras.layers.Dense(32, activation=tf.nn.relu))
        model.add(keras.layers.Dense(self.config['num_classes'], activation=tf.nn.softmax))
        self.model = model


class CNN1(mb.ImgClassifierModel):
    """
    A convolutional neural network model.
    """
    def __init__(self, datasets, num_classes, config):
        super().__init__(datasets, num_classes, config)
        self.model_name = 'CNN1'

        model = keras.Sequential()
        datasets = self.config['datasets']
        X = datasets['train_x']

        model.add(keras.layers.Conv2D(32, (3, 3), padding='same', input_shape=X.shape[1:]))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
        model.add(keras.layers.Conv2D(32, (3, 3), padding='same'))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Flatten())
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(self.config['num_classes'], activation=tf.nn.softmax))
        self.model = model


class CNN1BN(mb.ImgClassifierModel):
    """
    Same as CNN1, but with Batch normalization.
    """
    def __init__(self, datasets, num_classes, config):
        super().__init__(datasets, num_classes, config)
        self.model_name = 'CNN1BN'

        model = keras.Sequential()
        datasets = self.config['datasets']
        X = datasets['train_x']

        model.add(keras.layers.Conv2D(32, (3, 3), padding='same', input_shape=X.shape[1:]))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
        model.add(keras.layers.Conv2D(32, (3, 3), padding='same'))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Flatten())
        model.add(keras.layers.Dense(64))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.Dense(64))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))

        model.add(keras.layers.Dense(self.config['num_classes'], activation=tf.nn.softmax))
        self.model = model


class CNN1BNDr(mb.ImgClassifierModel):
    """
    Same as CNN1, but with Batch normalization.
    """
    def __init__(self, datasets, num_classes, config):
        super().__init__(datasets, num_classes, config)
        self.model_name = 'CNN1BNDr'

        model = keras.Sequential()
        datasets = self.config['datasets']
        X = datasets['train_x']

        model.add(keras.layers.Conv2D(32, (3, 3), padding='same', input_shape=X.shape[1:]))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.Dropout(0.25))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
        model.add(keras.layers.Conv2D(32, (3, 3), padding='same'))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.Dropout(0.25))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Flatten())
        model.add(keras.layers.Dense(64))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.Dropout(0.25))
        model.add(keras.layers.Dense(64))
        model.add(keras.layers.BatchNormalization())
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.Dropout(0.25))
        model.add(keras.layers.Dense(self.config['num_classes'], activation=tf.nn.softmax))
        self.model = model


class CNN2(mb.ImgClassifierModel):
    """
    A convolutional neural network model.
    """

    def __init__(self, datasets, num_classes, config):
        super().__init__(datasets, num_classes, config)
        self.model_name = 'CNN2'

        model = keras.Sequential()
        datasets = self.config['datasets']
        X = datasets['train_x']

        model.add(keras.layers.Conv2D(64, (3, 3), padding='same', input_shape=X.shape[1:]))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Conv2D(32, (3, 3), padding='same'))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Conv2D(32, (3, 3), padding='same'))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Flatten())
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(self.config['num_classes'], activation=tf.nn.softmax))
        self.model = model


class CNN3(mb.ImgClassifierModel):
    """
    A convolutional neural network model.
    """

    def __init__(self, datasets, num_classes, config):
        super().__init__(datasets, num_classes, config)
        self.model_name = 'CNN3'

        model = keras.Sequential()
        datasets = self.config['datasets']
        X = datasets['train_x']

        model.add(keras.layers.Conv2D(64, (3, 3), padding='same', input_shape=X.shape[1:]))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Conv2D(64, (3, 3), padding='same'))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Conv2D(32, (3, 3), padding='same'))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Conv2D(32, (3, 3), padding='same'))
        model.add(keras.layers.Activation(tf.nn.relu))
        model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        model.add(keras.layers.Flatten())
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(64, activation=tf.nn.relu))
        model.add(keras.layers.Dense(self.config['num_classes'], activation=tf.nn.softmax))
        self.model = model
