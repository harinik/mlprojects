import cifar_models as cm

MODELMAP = {
        'dnn1': cm.DNN1,
        'cnn1': cm.CNN1,
        'cnn1bndr': cm.CNN1BNDr,
        'cnn1bn': cm.CNN1BN,
        'cnn2': cm.CNN2,
        'cnn3': cm.CNN3,
}
