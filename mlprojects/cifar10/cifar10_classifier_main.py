"""Tool to run different models on the cifar10 dataset.
The dataset is not really restricted to cifar10, although, right now, I'm only focused on models
for that particular dataset.
"""
import sys

sys.path.append('../utils')
import argparse
import json
import logging
from tensorflow import keras

import modelreg as mr
import cifar_models as cm
import keras_utils


def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_type', type=str, help='Type of NN model for image classification with the CIFAR10 '
                                                     'dataset. Valid choices are: dnn1, cnn1, cnn1bn, cnn1bndr '
                                                     'cnn2, cnn3 or predict (for prediction)')

    parser.add_argument('--prediction_file', type=str, help='File to run prediction on.')
    parser.add_argument('-ih', '--img_height', type=int, default=32, help='Height of the image')
    parser.add_argument('-iw', '--img_width', type=int, default=32, help='Width of the image')
    parser.add_argument('-b', '--batch_size', type=int, help='batch size during training', default=64)
    parser.add_argument('-l', '--logfile', default='training.log', help='Log file to write to.')

    parser.add_argument('-e', '--num_epochs', type=int, default=10,
                        help='Number of epochs to run the training set for.')
    parser.add_argument('--augment_data', '-aug', type=bool, default=False,
                        help='Augment the cifar10 data for this model using keras ImageDataGenerator')
    parser.add_argument('model_file', type=str, help='File to save the model or load from, if it already exists.')
    parser.add_argument('labels_file', type=str,
                        help='File to write JSON representation of the human-readable labels')
    return parser.parse_args()


def classifier_main():
    args = read_args()
    print(args)
    logging.basicConfig(filename=args.logfile, level=logging.INFO)

    if args.model_type != 'predict':
        datasets = {}
        (datasets['train_x'], datasets['train_y']), (
        datasets['test_x'], datasets['test_y']) = keras.datasets.cifar10.load_data()

        print(cm.CATEGORIES)
        cat_json = json.dumps(cm.CATEGORIES)
        with open(args.labels_file, 'w') as cf:
            json.dump(cat_json, cf)

        config = {}
        config['epochs'] = args.num_epochs
        config['batch_size'] = args.batch_size
        config['augment'] = args.augment_data
        mod = None
        mc = mr.MODELMAP[args.model_type]
        if mc is not None:
            mod = mc(datasets, len(cm.CATEGORIES), config)

        if mod is not None:
            mod.train()
            mod.save(args.model_file)
    else:
        with open(args.labels_file) as cf:
            cat_json = json.load(cf)
        categories = json.loads(cat_json)
        print(categories)
        if args.prediction_file is None:
            print('Prediction file must be specified.')
            return
        model = keras.models.load_model(args.model_file)

        pred = keras_utils.keras_img_predict(model, args.prediction_file, args.img_height, args.img_width)
        print('Input file:', args.prediction_file)
        print('Predicted value:', categories[pred])


if __name__ == '__main__':
    classifier_main()
