import sys

sys.path.append('../utils')
import keras_utils
from tensorflow import keras
from sklearn.model_selection import train_test_split


class ImgClassifierModel(object):
    """
    Base class to encapsulate a Keras image classifier model.
    """

    def __init__(self, datasets, num_classes, config):
        baseconfig = {
            'epochs': 10,
            'batch_size': 64,
            'optimizer': keras.optimizers.Adam(lr=0.0001),
            'loss_fn': 'categorical_crossentropy',
            'metrics': ['accuracy'],
            'datasets': datasets,
            'num_classes': num_classes,
            'augment': False,
        }
        # merge the default config with the specified config.
        self.config = merge_config(baseconfig, config)
        self.model = None
        self.model_name = ''

    def train(self):
        """
        Trains the model.
        """
        # get the training, dev and test data sets
        datasets = self.config['datasets']
        datasets['train_x'] = datasets['train_x']/255.0
        datasets['test_x'] = datasets['test_x']/255.0
        datasets['train_y'] = keras_utils.convert_to_one_hot(datasets['train_y'], self.config['num_classes'])
        datasets['test_y'] = keras_utils.convert_to_one_hot(datasets['test_y'], self.config['num_classes'])
        print('Training model: ', self.model_name)
        self.model.summary()
        self.model.compile(optimizer=self.config['optimizer'], loss=self.config['loss_fn'],
                           metrics=self.config['metrics'])

        if self.config['augment']:
            train_X, val_X, train_Y, val_Y = train_test_split(datasets['train_x'], datasets['train_y'], test_size=0.1)
            print('train_X.shape: ', train_X.shape, ' train_Y.shape: ', train_Y.shape)
            print('val_X.shape: ', val_X.shape, ' val_Y,shape: ', val_Y.shape)
            train_dg = keras.preprocessing.image.ImageDataGenerator(rotation_range=20,
                                                                    width_shift_range=0.2,
                                                                    height_shift_range=0.2,
                                                                    horizontal_flip=True)

            val_dg = keras.preprocessing.image.ImageDataGenerator()

            history = self.model.fit_generator(train_dg.flow(train_X, train_Y,
                                                             batch_size=self.config['batch_size']),
                                               epochs=self.config['epochs'],
                                               steps_per_epoch=len(train_X) / self.config['batch_size'],
                                               validation_data=val_dg.flow(val_X, val_Y,
                                                                           batch_size=self.config['batch_size']),
                                               validation_steps=len(val_X) / self.config['batch_size'],
                                               verbose=2)

            keras_utils.show_keras_graph(history)
            test_dg = keras.preprocessing.image.ImageDataGenerator()

            test_loss, test_acc = self.model.evaluate_generator(test_dg.flow(datasets['test_x'], datasets['test_y']))
            print('test loss: ', test_loss, ' test accuracy: ', test_acc)
        else:
            history = self.model.fit(datasets['train_x'], datasets['train_y'],
                                     batch_size=self.config['batch_size'],
                                     epochs=self.config['epochs'],
                                     validation_split=0.1, verbose=2)

            keras_utils.show_keras_graph(history)
            test_loss, test_acc = self.model.evaluate(datasets['test_x'], datasets['test_y'])
            print('test loss: ', test_loss, ' test accuracy: ', test_acc)

    def save(self, model_file):
        self.model.save(model_file)


def merge_config(baseconfig, config):
    """
    Merges dicts baseconfig and config and returns a merged dict.
    The values in config override those present in baseconfig.
    """
    merged = {}
    for k, v in baseconfig.items():
        merged[k] = v
    for k, v in config.items():
        merged[k] = v
    return merged
