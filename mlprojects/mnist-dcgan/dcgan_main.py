import argparse
import dcgan


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--num_epochs', '-e', type=int, default=10, help='Number of epochs')
    parser.add_argument('--batch_size', '-b', type=int, default=128, help='Batch size')
    args = parser.parse_args()
    dcgan.run_train(args.num_epochs, args.batch_size)
